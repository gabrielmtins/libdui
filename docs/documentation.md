# Documentation
## Structures

### DUI\_Rect
```
typedef struct{
	int x, y, w, h;
} DUI_Rect;
```
A structure that represents a rect.

### DUI\_Color
```
typedef struct{
	uint8_t r, g, b, a;
} DUI_Color;
```
A structure that represents a color.

### DUI\_Context
```
typedef struct{
	uint8_t mouse_pressed;
	int mouse_xrel, mouse_yrel;
	int mouse_x, mouse_y;

	size_t num_windows;
	size_t focus;

	DUI_Rect *rect_focus;
	int rect_focus_has_title;
} DUI_Context;
```
A structure that has the information needed for the operation with
windows.

### DUI\_Window
```
typedef struct{
	DUI_Rect box;
	size_t num_widget_x;
	size_t num_widget_y;
	size_t widget_counter;

	DUI_Context* context;

	uint8_t focus_on_title_bar;
	uint8_t have_title_bar;
	size_t id;
} DUI_Window;
```
A structure that represents a window.

## Functions
### DUI\_CreateContext
`DUI_Context DUI_CreateContext(void);`

Returns a context structure.

### DUI\_CreateWindow
`DUI_Window DUI_CreateWindow(DUI_Context* context, DUI_Rect widget, size_t num_widget_x, size_t num_widget_y);`

Returns a window.  
The widget is a rect that contains the size and position of the window.  
The num\_widget\_x and num\_widget\_y are the number of widgets on the
horizontal axis and on the vertical axis.  
If num\_widget\_x or num\_widget\_y are equal to 0, then any widgets
inside the window will take up the whole window space.

### DUI\_UpdateContext
`void DUI_UpdateContext(DUI_Context* context, int mouse_pressed, int mouse_x, int mouse_y);`

Updates the context with the current mouse position and info.

### DUI\_UpdateCharTyped
`void DUI_UpdateCharTyped(DUI_Context* context, char c);`

Updates the last char typed.

### void DUI\_PopCharTyped
`void DUI_PopCharTyped(DUI_Context* context);`

Pops the last character.

### DUI\_PutWindow
`void DUI_PutWindow(DUI_Window* window, const char* title);`

Puts the window into the screen.

### DUI\_PutChildWindow
`void DUI_PutChildWindow(DUI_Window* parent_window, DUI_Window* child_window);`

Puts the child window inside parent window, as a widget of the parent window.

### DUI\_PutLabel
`void DUI_PutLabel(DUI_Window* window, const char* text);`

Puts a label into a window.

### DUI\_PutButton
`int DUI_PutButton(DUI_Window* window, const char* text);`

Puts a button into a window.  
Returns 1 if the button was pressed.

### DUI\_PutSliderHorizontal
`void DUI_PutSliderHorizontal(DUI_Window* window, double* value);`

Puts a horizontal slider into a window.  
The value is a pointer that contains a value between 0 and 1.

### DUI\_PutSliderVertical
`void DUI_PutSliderVertical(DUI_Window* window, double* value);`

Puts a vertical slider into a window.  
The value is a pointer that contains a value between 0 and 1.

### DUI\_PutTexture
`void DUI_PutTexture(DUI_Window* window, void* texture, double* x, double* y, int* mouse_pressed);`

Puts a texture into a window.  
The x and y pointers contains normalized values representing
where the texture was clicked.  

### DUI\_PutTextureAspectRatio
`void DUI_PutTextureAspectRatio(DUI_Window* window, void* texture, double aspect_ratio, double* x, double* y, int* mouse_pressed);`

Puts a texture into a window with aspect ratio aspect\_ratio.  
The x and y pointers contains normalized values representing
where the texture was clicked.  

### DUI\_PutListVertical
`int DUI_PutListVertical(DUI_Window* window, const char** list, size_t list_size);`

Puts a vertical list into a window.  
It returns the index of the list element that was selected by the user.  
It returns -1 if the user didn't select an item of the list.  
It returns -2 if the list is to big to fit in the whole window.

### DUI\_PutListHorizontal
`int DUI_PutListHorizontal(DUI_Window* window, const char** list, size_t list_size);`

Puts a horizontal list into a window.  
It returns the index of the list element that was selected by the user.  
It returns -1 if the user didn't select an item of the list.  
It returns -2 if the list is to big to fit in the whole window.

### DUI_\PutTextBox
`void DUI_PutTextBox(DUI_Window* window, char* typed_string, size_t max_len);`
Puts a text box into the screen.

### DUI\_Space
`void DUI_Space(DUI_Window* window, DUI_Color* color);`

Puts a space into a window of a certain color.  
If color is NULL, then it will just be a space with normal background color.
