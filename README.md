# libdui

libdui is a gui library for C.

This library is under zlib license.

## Install

```
$ make
$ sudo make install
```

## Details and development

I made this gui library because I wanted something simple to use in my
projects. It uses a SDL2 backend, but I made it so it is easy to write
another backend. I pretend to write a backend for xlib sometime later.
